# Alura
## Java
### Composicao + Static

Este exemplo mostra como fazer o uso de composicao e static.

### Composicao
Tomos como exemplo duas classes, 1 de Cliente e 1 de Conta.

A Classe conta tem um atributo Cliente chamado titular.

Quando uma classe tem como atributo uma outra classe dizemos que ela é composta.

É muito comum criar atributos do tipo String, String é uma classe em Java e muitos são compostar
por ela.

### Static

Digamos que você queira saber quantas contas foram criadas, ou seja, quantos new foram feitos para
instanciar uma classe do tipo Conta. Nesse caso, podemos usar uma variavel total em Conta do tipo private
e com static.

Podemos dizer que agora a variavel total pertence a Classe, nao mais a uma instancia da classe.
Qualquer instancia pode consultar a variavel total.

Como ela é private, foi criado um metodo getTotal que tambem é static e com isso, conseguimos chamar 
getTotal sem criar uma instancia.
ex: Conta.getTotal()...

