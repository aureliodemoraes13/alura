# Alura
## Java
### Heranca + Polimorfismo + uso Override + uso de Super

Esse simples projeto aborda coisas interessantes para quem quer iniciar no Java.

Há muitos tópicos na internet sobre o assunto, após esse overview, será possível ter um entendimento
mais claro sobre do que se trata o assunto.

### Herança
Podemos dizer que Herança ajuda na reutilazação de código e polimorfismo.
Tratando de reutilização da codigo podemos ver um exemplo tanto no package conta quando no package
funcionario.

Ex.: Todas as pessoas que trabalham em uma empresa são funcionários.
Gerente, Designer e editor de vídeos são funcionarios, por isso podemos dizer que eles herdam de
funcionario

Gerente extends Funcionario.

Quem extende de Funcionario, tem todos os atributos e metodos que o Funcionario tem e pode incluir mais
coisas.


Costumamos dizer que Funcionario é a classe mae e quem extende dela é a classe filha.

No caso do package Conta temos:
Conta, ContaCorrente e ContaPoupanca.

Conta é a classe mae, as outras sao classes filhas.

As classes filhas tem os metodos e os atributos da classe pai, por isso heranca ajuda na
*reutilizacao de codigo.*

*Polimorfismo*
Toda classe instanciada tem um tipo, mas quem faz referencia ela não necessariamente precisa ser
do mesmo tipo.

ex: Sabemos que Gerente extende de Funcionario, logo.

Funcionario f = new Gerente();

Em que contexto podemos usar Polimorfismo?

Bom, sabemos que um Funcionario tem um bonificacao, caso eu queira registrar a bonificacao de 
todos os funcionario, posso fazer igual na Classe ControleBonificacao.

Tem um metodo registra que recebe funcionario, desse funcionario consigo chegar até a bonificação dele.

Imagina que tem uma sala que nessa sala tem um rapaz da contabilidade, ele te chama e pergunta sua
bonificao, para ele nao importa se vc é gerente ou designer, ele so quer saber se vc é funcionario
e qual sua bonificao.

É isso que a Classe ControleBonificao faz, espera um funcionario.

como essa Funcionario pode ter varios tipos que herdam dele, dizemos que temos ai um *Polimorfismos*.


### Uso de Override

Vamos usar o metodo getBonificacao como exemplo.

Cada um tem uma bonificacao, mas todos eles herdam de Funcionario que ja tem um getBonificao padrao.

Podemos ir ate uma outra classe que extende Funcionario e reescrever esse metodo.

usamos a anotação @Override para nos auxiliar nessa alteração.

O uso dessa anotação não é obrigatória, mas ajuda.

Sempre que vc ver um Override lembre-se, o método que carrega essa antação esta alterando o comportamento
do metodo da Classe Mae.

### O que é super?
Super é a forma como a classe filha representa sua classe Mae.

Usando como exemplo as contas. Podemos ver que a Classe conta tem um construtor que recebe argumentos.

Toda conta, para ser criada, precisa de agencia e conta.

Uma vez que vc herda de conta, como é o caso dao Conta Corrente, vc também precisa passar esses dados.
No entando, *Construtores não são herdados*, isso significa que vc precisa escrever seu próprio construtor.

O contrutor da classe filha vai ter uma assiatura igual da classe Mae, nesse caso sem a anotação @Override.
Dentro desse contrutor vc passa esses dados para a Mae, usando o super.

exemplo:
public ContaCorrente(int agencia, int conta){
  super(agencia, conta);
}
 
ao extender de uma classe que não tem construtor padrao, sera lançado um erro na IDE, dizendo que o Super
implicitamente nao foi atendido.



Para saber mais.

É comum em projetos java ver a palavra reservada super e this.

*this* => Representa a propria classe, uma instancia da propria classe.
*super* => Representa a classe mae.









