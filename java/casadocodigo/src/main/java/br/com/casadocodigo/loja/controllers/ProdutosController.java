package br.com.casadocodigo.loja.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import br.com.casadocodigo.loja.dao.ProdutoDAO;
import br.com.casadocodigo.loja.models.Produto;
import br.com.casadocodigo.loja.models.TipoPreco;

import javax.validation.Valid;

@Controller
@RequestMapping("produtos")
public class ProdutosController {
	
	@Autowired
	private ProdutoDAO dao;

	@RequestMapping(value="/form", method=RequestMethod.GET)
	public ModelAndView form(Produto produto) {
		ModelAndView modelAndView = new ModelAndView("produtos/form");
		modelAndView.addObject("tipos", TipoPreco.values());
		modelAndView.addObject("produto", produto);

		return modelAndView;
	}
	
	@RequestMapping(value="/form", method=RequestMethod.POST)
	public ModelAndView gravar(@Valid Produto produto,BindingResult bindingResult,
			RedirectAttributes redirectAttributes){
		
		if (bindingResult.hasErrors()) {
			return form(produto);
		}
		
		dao.gravar(produto);
		
		redirectAttributes.addFlashAttribute("mensagem", "Novo livro salvo com sucesso!");
		
		return new ModelAndView("redirect:/produtos");
	}
	
	@RequestMapping( method=RequestMethod.GET)
	public ModelAndView listar() {
		List<Produto> produtos = dao.listar();
		ModelAndView modelAndView = new ModelAndView("produtos/lista");
		modelAndView.addObject("produtos", produtos);
		return modelAndView;
	}
}
