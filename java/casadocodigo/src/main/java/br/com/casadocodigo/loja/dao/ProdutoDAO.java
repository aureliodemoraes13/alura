package br.com.casadocodigo.loja.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import br.com.casadocodigo.loja.models.Produto;

@Repository
@Transactional
public class ProdutoDAO {

	@PersistenceContext
	private EntityManager manager;
	
	public void gravar(Produto produto) {
		manager.persist(produto);
	}

	public List<Produto> listar() {
		
		String jpql = "select p from Produto p";
		TypedQuery<Produto> typedQuery = manager.createQuery(jpql, Produto.class);
		typedQuery.setHint("org.hibernate.cacheable", "true");
		List<Produto> listaDeProdutos = typedQuery.getResultList();
		return listaDeProdutos;
		
		//return em.createQuery("from Produto", Produto.class).getResultList();
		
	}
}