# Alura
## Java
### Pilha e Exceções

**Pilha:** Podemos dizer que os metodos e classes em java tem um ordem para serem executados. Conforma são executados vão sendo includos na pilha de execução. Pilha também é chamada de stack.

Digamos que temos o Metodo A que chama o Metodo B que chama o metodo C que finaliza.
Nesse caso, temos o conceito de pilha FILO ( First in, last out )

Ou seja: A >> B >> c
Assim que C finalizar retorna para B, assim que B finalizar retorna para A que termina o programa.

Um erro muito comum quando começamos a trabalhar com recursividade é o stackoverflow, a pilha ficou tão cheia que estourou a capacidade da memória.

no arquivo FluxoComErro.java temos um exemplo. O metodo A chama o B que chama o C e fica chamando o C infinitamete. Nesse caso, teremos essa exceção.


**Exceções:** Algumas exceções são bem conhecidas por que acontecem muuuuuito quando estamos iniciando com java. Além da stackoverflow comentada acima temos a NullPointerException. Isso acontece quando estamos querendo acessar uma propriedade de um objeto que não foi instanciado.

EX: Sabemos que todo funcionario tem um nome.
Funcionario func.

Com isso, fazermos: func.getNome();

Porém, a variável func não está fazendo referencia para nenhum dado na memória, apenas foi criada mas não instanciada.

Como tratar as exception para não ficar quebrando o código e mostrar uma mensagem tosca para o usuário?

Bom, o ideal é fazer um try catch.
Tenatar executar esse código, se ele quebrar, pega esse erro trata aqui do jeito que eu quero e continua a execução do código.


No arquivo Fluxo.java da para ver como funciona. Foi usado o erro NullPointerException como exemplo.

_______________________________

Exception, RunTimeException, NullPointerException. O que tudo isso quer dizer?
Já ouvir falar de herança? Então, esse é um bom exemplo de como o java usa herança.

A classe mais generica de todas é a classe Throwable. Depis vem a Classe Exception, RuntimeException e por fim nullpointerexception, stackoverflow.

Throwable << Exception << RuntimeException << NullPointerException.

Exception cobre todos os tipos de erro. Qualquer erro que for lançado dentro do try será pego no catch caso esteja no level Exception.

NullPointerException pega apenas casos em que tentamos acessar um atributo de uma classe que não foi instanciada.

Será que é melhor sempre usar Exception então?
Não. Tudo tem um custo de processamento, quando vc herda, não é diferente, além d mais, seu código fica mais legível quando você diz que esta esperando um erro de um tipo específico e não tão generico.

__________________________________
Para 1 try podemos colocar quantos catch's forem necessários.

Com o java 7 podemos usar apenas 1 catch e declaras o que estamos esperando naquele bloco como erro.

EX:  catch(ArithmeticException | NullPointerException ex) 
O que acontece dentro do try pode retornar um ArithmeticException, caso tentar dividir numero por zero.
pode ser visto no arquivo FluxoComTratamento.java

_____________________________________

Outro bloco muito importante é o Finally, esse é opcional.
devemos usar try ou com catch ou com Finally

e caso esteja usando try com catch, pode incluir finally também.


Digamos que tenhams alguma falha no bloco try. Quam vai tratar é o catch, e se der falha no catch, ai quem trata é o finally.

Outro ponto também que o o Finally é muito usada para fechar conexões e arquivos.

Foi pensando nisso que a comunidade, no java 7 lançou o try-with-resources.


VOcê consegue abrir uma conexão e nem precisa ser preocurar em fechala. O java se encarrega de fazer isso pra vc. Mas para isso vc a classe que vai no try precisa implementar a classe AutoCloseable e sobrescrever o metodo close.



**Criando sua propria exception**
A comunidade e os responsaveis pelos frameworks ja pensaram em diversas exception que pode ser lançadas.
Vc também pode criar a sua mas isso é desencorajado, procure uma que já exista e se encaixe no seu escopo.

Nos arquivo te um exemplo de como é uma classe criada.

Basicamente é só extender de um outra classe de erro e implementar o que ela pedir.

VC pode extender de Exception ou de RuntimeException.


Nesse caso, entramos em um outro item

**Checked e Unchecked Exception**
A classe Exception e as exceções que ela cobre é do tipo Checked, ou seja, vc precisa avisa para o compilador que aquele pedaço de código pode lançar uma exceção.

E ai nesse caso ou vc coloca uma anotação no metodo dizendo que ele throws Exception ... ou vc envolve seu código em um try catch.

A classe RuntimeException é do tipo Unckecked então vc não precisa avisar o compilador para usar nullpointerexception ou arithmaticexception.

No caso das exceções checked, nem compila se não colocar um try catch ou anotação throws

  
