# Alura
## Java
### Uso de Classe Abstrata e Interface.

Aprender sobre classe abstrata e Interfaces é criar a base para aprender muitos outros conceitos
importantes sobre Java.

Tdas as bibliotecas java fazem use ou de interface ou herança.

Nesse projeto há dois exemplo sobre esse uso, um com funcionarios de uma empresa e um com tipos de conta.

Esse tópico é bem legal e para ter um entendimento completo sobre ele, o ideal é ver vídeos no youtube para
poder entender o assunto.

Apenas para dar uma passada pelo tópico, podemos falar sobre o uso de cada um desses conceitos.

Sabemos que em uma empresa, todos são funcionarios. Porem, para o RH ou para o contador, ser apenas um
funcionario não quer dizer muita coisa, você deve dizer se é um Gerente, Editor de Vídeo, etc.

Por isso, no nosso sistema, não faz sentido dar new em um Funcionario, posso sim ter um Gerente do tipo
Fucionario, mas não ter uma instancia de Funcionario. É nesses casos que dizermos que funcionario que algo
abstrato e não concreto.

O mesmo vale para uma conta de banco. Para o banco, somente conta não quer dizer muita coisa. Preciso saber
qual é o tipo dessa conta, conta corrente ou conta poupança, etc. Logo somente conta é abstrato.

E por que ter uma classe abstrata? Para ganharmos o polimorfismo.

Para o banco, quando for preciso fazer algum tipo de tributação eu preciso receber uma conta, uma coisa mais
generica.

Quando o responsavel do contador quiser calcula a bonificação ele esperar por um funcionario. algo mais 
generico.


Já o uso de Interface nos leva a um outro patamar.
Nem sempre conseguimos fazer o uso de herença
Ex:
No sistema interno da empresa, somente alguns funcionarios podem se autenticar num determinado sistema.
Porem, os cliente também podem se autenticar nesse sistema.

Posso criar uma interface de autenticacao, tanto um funcionario quanto cliente implementao essa interface.

No sistema de autenticação, posso receber como paramentro algo do tipo autenticavel, tanto faz se for 
cliente ou funcionario.

Em resumo, queria dixar um visao que:
Herança é usado para reutilização de código e polimorfismo
Composição é usado para reutilização de código
Interface é usado como polimorfismo.


A utilização de Herança sempre foi desencorajada pois herança = acoplamento e a manutenção pode se
tornar um problema.

Procure usar interface e composição alinhado com classe e metodos abstratos quando for necessario.


